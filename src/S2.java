import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

public class S2 extends JFrame {

    JTextArea input;
    JButton btn;


    public S2(){
        setTitle("Write to file");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,400);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        input = new JTextArea();
        input.setBounds(10,10,100,50);

        btn = new JButton("Write to file");
        btn.setBounds(10,70,100,50);
        btn.addActionListener(new ButtonAction());

        add(input);add(btn);

    }

    public static void main(String[] args) {
        new S2();
    }

    class ButtonAction implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String file = "file.txt";

            try {
                FileWriter writer = new FileWriter("C:\\Users\\Stefan\\" +
                        "Desktop\\examen_se_2021\\src\\" + file); //Had to copy the absolute path here
                writer.write(input.getText());                    //in order for it to see the file
                writer.close();

            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
