public class S1 {

    public class I extends U{  //inheritance
        private long t;
        public K k;   //association to K

        public void f(){
            System.out.println("f works");
        }

        public void i(J j){     //dependency to J
            System.out.println(j.toString() + " works");
        }

        public void p(){
            System.out.println("p from I works");
        }
    }

    public class J{

    }

    public class N{
       public I i;   //association ,doesn't specify access type so I selected public

    }

    public class L{

        public void metA(){
            System.out.println("metA works");
        }
    }

    public class K{
         L l;

        public K(){
            l = new L();   //composition to L
        }
    }

    public class S{

        K k = new K();   //aggregation to K

        public void metB(){
            System.out.println("metB works");
        }
    }

    public class U{

        public void p(){
            System.out.println("p from U works");
        }
    }
}